import React, { Component } from 'react';
import {  HashRouter, Route, Switch } from 'react-router-dom';
// import { renderRoutes } from 'react-router-config';
import './App.scss';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));
const DefaultLayoutAdmin = React.lazy(() => import('./containers/DefaultLayout/Adminsite/DefaultLayout'));

// Pages Admin
const Login = React.lazy(() => import('./views/Pages/Login'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Forgetpassword = React.lazy(() => import('./views/Pages/Forgetpassword'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));

// Pages User

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const ApiDocumentation = React.lazy(() => import('./views/ApiDocumentation'));
const Documentation = React.lazy(() => import('./views/Documentation'))
const DetilDocumentation = React.lazy(() => import('./views/Documentation/Detil'))
const Search = React.lazy(() => import('./views/Search'))

class App extends Component {

  render() {
    return (
          <React.Suspense fallback={loading()}>
            <HashRouter basename={'/docs'}>
              <Switch>
              <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/Forgetpassword" name="Forgetpassword Page" render={props => <Forgetpassword {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
              <Route path="/adminsite" name="Home" render={props => <DefaultLayoutAdmin {...props}/>} />
              <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
              </Switch>
            </HashRouter>
          </React.Suspense>
    );
  }
}


  
//   render() {
//     return (
//           <React.Suspense fallback={loading()}>
//              <HashRouter basename={'/docs'}>
//               <Switch>
//               <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
//               <Route path="/dashboard" name="Dashboard" render={props => <Dashboard {...props}/>} />
//               <Route path="/api_documentation/:id" name="API Documentation" render={props => <ApiDocumentation {...props}/>} />
//               <Route path="/documentation/:category/:id?" name="Documentation" render={props => <Documentation {...props}/>} />
//               <Route path="/search" name="Search" render={props => <Search {...props}/>} />
//               </Switch>
//               </HashRouter>
//             </React.Suspense>
           
//     );
//   }
// }

export default App;
