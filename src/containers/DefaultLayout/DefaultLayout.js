import React, { Component, Suspense } from 'react';
import { HashRouter, Redirect, Route, Switch, Link } from 'react-router-dom';
import * as router from 'react-router-dom';
import {APP_TITLE, API_CATEGORY, API_DOC_LIST} from '../../Constants';
import { getUrlParameter } from '../../Helper'
import {
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import {
  Container,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Button
} from 'reactstrap';
// sidebar nav config
//import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import axios from 'axios'

const DefaultAside = React.lazy(() => import('./DefaultAside'));
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
  constructor(props){
    super(props)
    this.state = {
      // coba disini
      rawData: [],
      navigation: {items: []},
      // sampai sini
      categoryById: [],
      categoryBreadcrump: [],
      activeCategory: null,
      activeDocumentName: null,
      activeBreadcrump: [],
      activeKeyword: getUrlParameter("keyword"),
      inputKeyword: getUrlParameter("keyword"),
      reload: false,
      loading: true
    }
  }
  componentDidUpdate(prevProps) {
    if ( this.props.location.pathname !== prevProps.location.pathname && prevProps.location.pathname === "/search") {
      this.setState({
        activeKeyword: null,
        inputKeyword: null
      })
    } else if ( this.props.location.pathname !== prevProps.location.pathname && this.props.location.pathname === "/search") {
      let currentKeyword = getUrlParameter("keyword")
      this.setState({
        activeKeyword: currentKeyword,
        inputKeyword: currentKeyword
      })
    }
  }
  shouldComponentUpdate() {
    return true;
  }
  componentDidMount() {
    this.fetchingData()
  }

  fetchingData = (param) => {
    let url = `${API_CATEGORY}`
    axios.get(url)
    .then(response => {
      const categoryListById = this.gCatListById(response.data);
      const categoryBreadcrump = this.gCatBreadcrumpById(response.data, [], categoryListById);

      let categoryList = [{
        name: 'Home',
        url: '/',
        icon: 'icon-speedometer',
      }]
      const addFixedMenu = () => {
        for (let i =0; i<this.fRec(response.data).length; i++){
          categoryList.push((this.fRec(response.data))[i])
        }
        return categoryList
      }
      this.setState({
        navigation: {items: addFixedMenu()},
        categoryById: categoryListById,
        categoryBreadcrump: categoryBreadcrump,
        loading: false
      })

    })
    .catch(error => {
      this.setState({
        loading: false
      })
      console.log("Error Fetching Data Category",error)
    })
  }

  gCatBreadcrumpById = (data, currentArray, dataById) => {
    currentArray = typeof currentArray === "undefined" ? []: currentArray;
    data.map(item => {
      currentArray["category_"+item.id] = this.fBreadcrumpCategory(item.id, item, [], dataById)
      if (item.category){
        currentArray = this.gCatBreadcrumpById(item.category, currentArray, dataById)
      }
    });
    return currentArray;
  }

  gCatListById = (data, currentArray) => {
    currentArray = typeof currentArray === "undefined" ? []: currentArray;
    data.map(item => {
      currentArray["category_"+item.id] = item
      if (item.category){
        currentArray = this.gCatListById(item.category, currentArray)
      }
    });
    return currentArray;
  }

  fRec = (data) => {
    return (
      data.map(item => {
        if (item.category){
          return {
            name: item.category_name,
            url: `/documentation/${item.id}`,
            icon: `icon-folder`,
            children: this.fRec(item.category),
          }
        }
        return {
          name: item.category_name,
          url: '/documentation/'+ item.id,
          icon: `icon-folder`,
        }
      })
    )
  }

  fBreadcrumpCategory = (parentID, parentData, currentArray, dataById) => {
    currentArray = typeof currentArray === "undefined" ? []: currentArray;
    const objectParentCategory = {
      id: parentData.id,
      name: parentData.category_name,
    };
    currentArray.push(objectParentCategory);
    if ( parentData !== null ) {
      if ( parentData.parent_id !== null ) {
        const nextParent = this.fBreadcrumpCategory(parentData.parent_id, dataById["category_"+parentData.parent_id], currentArray, dataById);
        currentArray = nextParent;
      } else {
        let finalArrayResult = [];
        for ( let i=(currentArray.length-1); i>=0; i-- ) {
          finalArrayResult.push(currentArray[i]);
        }
        return finalArrayResult;
      }
    } 
    return currentArray
  }

  generateBreadCrump() {
    const {activeBreadcrump} = this.state;
    
    return (
      <ol className="breadcrumb">
        <li className="breadcrumb-item"><Link to={`/dashboard`}>Home</Link></li>
        {
          activeBreadcrump.map((item, index) => {
            return (
              <li className="breadcrumb-item" key={index}>
                {
                  item.link === null ? item.label
                  : <Link to={item.link}>{item.label}</Link>
                }
              </li>
            )
          })
        }
      </ol>
    )
  }
  chooseCategory = id => {
    const {categoryById, categoryBreadcrump, activeDocumentName} = this.state
    let breadcrumbForCategory = []
    let breadcrumb = []
    if ( id !== null ) {
      breadcrumbForCategory = typeof categoryBreadcrump["category_"+id] === "undefined" ? []: categoryBreadcrump["category_"+id]
    }

    breadcrumbForCategory.map((item, index) => {
      if ( index === breadcrumbForCategory.length-1 && activeDocumentName === null ) {
        breadcrumb.push({
          label: item.name,
          link: null
        })
      } else {
        breadcrumb.push({
          label: item.name,
          link: `/documentation/${item.id}`
        })
      }
    })

    this.setState({
      activeCategory: id, 
      activeDocumentName: null, 
      activeBreadcrump: breadcrumb
    })
    if ( typeof categoryById["category_"+id] !== "undefined" ) 
      document.title = categoryById["category_"+id].category_name + " | " + APP_TITLE
    else
      document.title = "404 | "+ APP_TITLE
  }
  setActiveDocuments = documentName => {
    const {categoryById, activeCategory, categoryBreadcrump} = this.state
    let breadcrumbForCategory = []
    let breadcrumb = []
    if ( activeCategory !== null ) {
      breadcrumbForCategory = typeof categoryBreadcrump["category_"+activeCategory] === "undefined" ? []: categoryBreadcrump["category_"+activeCategory]
    }

    breadcrumbForCategory.map((item, index) => {
      if ( index === breadcrumbForCategory.length-1 && documentName === null ) {
        breadcrumb.push({
          label: item.name,
          link: null
        })
      } else {
        breadcrumb.push({
          label: item.name,
          link: `/documentation/${item.id}`
        })
      }
    })
    if ( documentName !== null ) 
      breadcrumb.push({
        label: documentName,
        link: null
      })

    this.setState({
      activeDocumentName: documentName, 
      activeBreadcrump: breadcrumb
    });
    if ( documentName !== null )
      this.setDocumentTitle(documentName + " | " + APP_TITLE)
    else if ( typeof categoryById["category_"+activeCategory] !== "undefined" ) 
      this.setDocumentTitle(categoryById["category_"+activeCategory].category_name + " | " + APP_TITLE)
    else
      this.setDocumentTitle("404 | "+ APP_TITLE)
  }
  setDocumentTitle = title => {
    document.title = title + " | " + APP_TITLE
  }
  setBreadcrump = (breadcrumb) => {
    this.setState({activeBreadcrump: breadcrumb})
  }
  handleSearchChange = (event) => {
    this.setState({inputKeyword: event.target.value})
  }
  searchDocuments = (e) => {
    e.preventDefault()
    const {activeKeyword, inputKeyword} = this.state
    if ( inputKeyword !== null && inputKeyword !== "" ) {
      if ( inputKeyword === activeKeyword ) {
        this.setState({reload: true}, () => {
          this.setState({reload: false})
        })
      } else {
        this.setState({activeKeyword: inputKeyword}, () => {
          this.props.history.push('/search?keyword='+escape(inputKeyword))
        })
      }
    }
    return false
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  pageLoading = () => <div className="page_loader"><img src="/assets/img/page_loader.gif" /></div>

  render() {
    const {loading, navigation, activeCategory, reload, activeKeyword, inputKeyword} = this.state
    return (
      // <React.Fragment>
        <HashRouter>
        <>
        {
          loading 
          ? this.pageLoading(): (
            <div className="app">
              <AppHeader fixed>
                <Suspense  fallback={this.loading()}>
                  <DefaultHeader/>
                </Suspense>
              </AppHeader>
              <div className="app-body">
                <AppSidebar fixed display="lg">
                  <AppSidebarHeader />
                  <div className="sidebar-form" style={{padding: "15px 15px"}}>
                    <Form onSubmit={this.searchDocuments.bind(this)}>
                      <InputGroup>
                          <Input key={`keyword:${activeKeyword}`} type="text" name="keyword" id="keyword" defaultValue={inputKeyword} innerRef={inputKeyword} placeholder="Search Documents..." onChange={(e) => this.handleSearchChange(e)} required={true} />
                          <InputGroupAddon addonType="prepend">
                            <Button className="btn-xs input-group-text" type="submit"><i className="fa fa-search"></i></Button>
                          </InputGroupAddon>
                      </InputGroup>
                    </Form>
                  </div>
                  <Suspense>
                  <AppSidebarNav navConfig={navigation} {...this.props} router={router}/>
                  </Suspense>
                  <AppSidebarFooter />
                  <AppSidebarMinimizer />
                </AppSidebar>
                <main className="main">
                  <nav className="" aria-label="breadcrumb">
                    {this.generateBreadCrump()}
                  </nav>
                  <Container fluid>
                    <Suspense fallback={this.loading()}>
                        <Switch>
                        {routes.map((route, idx) => {
                          return route.component ? (
                            <Route
                              key={idx}
                              path={route.path}
                              exact={route.exact}
                              name={route.name}
                              render={props => (
                                <route.component 
                                  reload={reload}
                                  chooseCategory={this.chooseCategory.bind(this)} 
                                  setActiveDocuments={this.setActiveDocuments.bind(this)}
                                  setDocumentTitle={this.setDocumentTitle.bind(this)}
                                  setBreadcrump={this.setBreadcrump.bind(this)}
                                  {...props} />
                              )}
                              />
                          ) : (null);
                        })}
                        </Switch>
                    </Suspense>
                  </Container>
                </main>
                <AppAside fixed>
                  <Suspense fallback={this.loading()}>
                    <DefaultAside />
                  </Suspense>
                </AppAside>
              </div>
              <AppFooter>
                <Suspense fallback={this.loading()}>
                  <DefaultFooter />
                </Suspense>
              </AppFooter>
            </div>
          )
        }
        </>
        </HashRouter>
      // </React.Fragment>
     
      

    )
  }
}

export default DefaultLayout;
