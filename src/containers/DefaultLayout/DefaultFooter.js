import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { HashRouter } from 'react-router-dom';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <HashRouter>
      <React.Fragment>
        <span>Doc App &copy; 2020</span>
        <span className="ml-auto">Powered by Direktorat Jendral Bea dan Cukai</span>
      </React.Fragment>
      </HashRouter>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
