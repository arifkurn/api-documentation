import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route} from 'react-router-dom';
import DefaultLayout from '../DefaultLayout';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<HashRouter basename={'/docs'}><Route path="/" name="Home" component={DefaultLayout} /></HashRouter>, div);
  ReactDOM.unmountComponentAtNode(div);
});
