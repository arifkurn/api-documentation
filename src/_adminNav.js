export default {
  items: [
    {
      name: 'Dashboard',
      url: '/adminsite/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'Category',
      url: '/adminsite/category',
      icon: 'icon-list'
    },
    {
      name: 'Documentation',
      url: '/adminsite/documentation',
      icon: 'icon-list'
    }
  ]
};