import React from 'react';
import { HashRouter } from 'react-router-dom';
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const ApiDocumentation = React.lazy(() => import('./views/ApiDocumentation'));
const Documentation = React.lazy(() => import('./views/Documentation'))
const DetilDocumentation = React.lazy(() => import('./views/Documentation/Detil'))
const Search = React.lazy(() => import('./views/Search'))

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard,  },
  { path: '/api_documentation/:id', name: 'API Documentation', component: ApiDocumentation,  },
  { path: '/documentation/:category/:id?', name: 'Documentation', component: Documentation,  },
  { path: '/search', name: 'Search Result', component: Search,  }
];

export default routes;