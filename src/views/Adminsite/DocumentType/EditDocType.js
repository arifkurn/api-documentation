import React, { useState } from "react";
import dynamic from 'next/dynamic';
import 'react-quill/dist/quill.snow.css';
import Axios from "axios";
import swal from 'sweetalert';
import {Card,CardHeader, CardBody,CardFooter,Row,Col} from 'reactstrap'
import { API_UPLOAD_FILE, API_DOCTYPE } from "../../../Constants";
const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });


class EditDocType extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    
        this.state = {
          inputDocType: this.props.type,
          editorHtml: this.props.template,
          id:this.props.match.params.id,
          
        };
      }
      
    
      onChangeHandler = e => {
        this.setState({ [e.target.name]: e.target.value });
        console.log(e.target.value)
      };
    
    
      handleChange(html) {
        this.setState({ editorHtml: html });  
      }
    
      onSave=()=>{
        const { match: { params }, history } = this.props

        let form = {
          type: this.state.inputDocType,
          template: this.state.editorHtml,
         
        };
        
        const options = {
          headers: {
            'Content-Type': 'application/json'
          }
        }
        // let {index} = this.props.match.params;
       
        Axios.put(API_DOCTYPE + `?id=${this.state.id}`, form, options)
          .then((res) => {
            swal({
              title: "Success",
              text: "Data Berhasil di Edit",
              icon: "success",
              button: "OK",
            }).then(res=>{
                history.push(`/adminsite/documentType`)
            }) 
          })
          .catch(error => {
            console.log(error)
          })
      }
    
      imageHandler() {
        const input = document.createElement("input");
    
        input.setAttribute("type", "file");
        input.setAttribute("accept", "image/*");
        input.click();
        const apiPostNewsImage = async (formData) => {
          return await Axios(
            {
              method: 'post',
              url: API_UPLOAD_FILE,
              data: formData,
              headers: {
              'content-type': `application/json`,
              }
            }
          ).then(function (response) {
            if ( typeof response.data.fileViewUri !== "undefined" ) {
              console.log("URI", response.data.fileViewUri)
              return response.data.fileViewUri
            } else {
              alert("Failed upload image to server")
              return null
            }
          })
          .catch(function (err) {
              alert("ERROR: "+err)
              return null
          });
        }
        input.onchange = async () => {
          const file = input.files[0];
          const formData = new FormData();
    
          formData.append("file", file);
    
          // Save current cursor state
          const range = this.quill.getSelection(true);
    
          // Insert temporary loading placeholder image
          this.quill.insertEmbed(
            range.index,
            "image",
            `${window.location.origin}/images/loaders/placeholder.gif`
          );
    
          // Move cursor to right side of image (easier to continue typing)
          this.quill.setSelection(range.index + 1);
    
          const res = await apiPostNewsImage(formData);
          // Remove placeholder image
          this.quill.deleteText(range.index, 1);
    
          // Insert uploaded image
          // this.quill.insertEmbed(range.index, 'image', res.body.image);
          this.quill.insertEmbed(range.index, "image", res);
        };
      }
    
      render() {
          console.log(this.props)
          const edit = this.props.location.state
        return (
          <React.Fragment>
            <Card>
              <CardHeader>
                <h3>Edit Document Type</h3>
              </CardHeader>
              <CardBody >
                  <Row>
                  <Col xs="12">
                    <label>Type</label>
                    <input 
                    type="text" 
                    className="ml-3 form-control" 
                    name="inputDocType" 
                    onChange={this.onChangeHandler}
                    defaultValue={edit.typeedit}
                    />
                  </Col>
                  </Row>
                  <Row>
                  <Col xs="12">
                  <label>Template</label>
                  <ReactQuill
                        theme="snow"
                        ref={el => {
                            this.quill = el;
                        }}
                        onChange={this.handleChange}
                        defaultValue={edit.templateedit}
                        placeholder={this.props.placeholder}
                        modules={{
                            toolbar: {
                                container: [
                                    [{ header: '1' }, { header: '2' }, { header: [3, 4, 5, 6] }, { font: [] }],
                                    [{ size: [] }],
                                    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                                    [{ list: 'ordered' }, { list: 'bullet' }],
                                    ['link', 'video'],
                                    ['link', 'image', 'video'],
                                    ['clean'],
                                    ['code-block']
                                ],
                                handlers: {
                                    image: this.imageHandler
                                }
                            }
                        }}
                    />
                  </Col>
                  </Row>
              </CardBody>
              <CardFooter>
                <button className="btn btn-primary" onClick={this.onSave}>Save</button>
              </CardFooter>
            </Card>
          </React.Fragment>
        );
      }
    }

export default EditDocType;