import React, { Component } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Table,
} from "reactstrap";
import axios from "axios";
import {
  API_DOCTYPE,
} from "../../../Constants";
import { Link, NavLink } from "react-router-dom";
import swal from "sweetalert";
import AddDocType from "./AddDocType";
import EditDocType from "./EditDocType";

class DocumentType extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      docList: [],
      isLoading: true,
      id: null,
      showDocumentationPage: false,
      activeDocumentation: false,
      idedit: null,
      templateedit: null,
      typeedit: null
    };
  }

  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  fetchingData() {
    const { showDocumentationPage } = this.state;
    let url = `${API_DOCTYPE}`;
    axios
      .get(url)
      .then((response) => {
        
        let activeDocumentationData = false;
        if (showDocumentationPage) {
          const indexDoc = this.props.match.params.id;
          activeDocumentationData =
            typeof response.data[indexDoc] === "undefined"
              ? false
              : response.data[indexDoc];
          if (activeDocumentationData)
            this.props.setActiveDocuments(activeDocumentationData.type);
          else if (indexDoc === "add")
            this.props.setActiveDocuments("Add New Document Type");
          else this.props.setActiveDocuments("404");
        }

        this.setState({
          docList: response.data,
          isLoading: false,
          activeDocumentation: activeDocumentationData,
        });
      })
      .catch((error) => {
        this.setState({
          docList: [],
          isLoading: false,
        });
        console.log(`parsing data from ${API_DOCTYPE} failed`, error);
      });
  }

  onBtnDelete = (id) => {
    swal({
      title: "Are you sure?",
      text: "Apakah Anda Yakin Akan Menghapus Dokumentasi ini ??",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        axios
          .delete(API_DOCTYPE + "?id=" + id)
          .then((res) => {
            this.fetchingData();
            console.log(res.data);
          })
          .catch((err) => {
            console.log(err);
          });
        swal("Data berhasil di hapus", {
          icon: "success",
        });
      } else {
        swal("Data berhasil di simpan");
      }
    });
  };

  componentDidMount() {
    const documentationID = this.props.match.params.id;
    if (typeof documentationID !== "undefined" && documentationID !== null)
      this.setState(
        { showDocumentationPage: true},
        () => {
          this.fetchingData();
          
        }
      );
    else
      this.setState(
        { showDocumentationPage: false },
        () => {
          this.fetchingData();
        }
      );
    this.fetchingData();
    
  }

  componentDidUpdate(prevProps) {
    const { docList } = this.state;
    const documentationID = this.props.match.params.id;
    const doctypeid = this.props.match.params.category;
  
    
    if (typeof documentationID !== "undefined" && documentationID !== null) {
      if (this.props.match.params.id !== prevProps.match.params.id) {
        let activeDocumentationData = false;
        let isLoading = true;
        if (this.props.match.params.category === prevProps.match.params.category) {
          activeDocumentationData = typeof docList[documentationID] === "undefined" ? false : docList[documentationID]
          if (activeDocumentationData)
            this.props.setActiveDocuments(activeDocumentationData.type);
          else if (documentationID === "add")
            this.props.setActiveDocuments("Add New Document Type");
          else if (documentationID === "edit")
            this.props.setActiveDocuments("404");

          isLoading = false;
        } else {
          this.props.setActiveDocuments(null);
        }
        this.setState(
          {
            isLoading: isLoading,
            activeDocumentation: activeDocumentationData,
            showDocumentationPage: false,
            
          }, () =>{
            if (this.props.match.params.category !== prevProps.match.params.category) {
              this.fetchingData();
              this.props.documentationData(this.props.match.params.category);
              // this.props.getCategorybyID(this.props.match.params.category)
            }
          }
          
        );
      } 
    } else if (this.props.match.params.id !== prevProps.match.params.id) {
      this.props.setActiveDocuments(null);
      this.setState(
        { showDocumentationPage: false, showCategoryPage: true }, () => {
            if (this.props.match.params.category !== prevProps.match.params.category) {
                this.fetchingData();
                this.props.documentationData(this.props.match.params.category)
        }
        })
    }
  }

  UNSAFE_componentWillMount() {
    const documentationID = this.props.match.params.id;
    if (typeof documentationID !== "undefined" && documentationID !== null)
      this.setState({  showDocumentationPage: true });
    else
      this.setState({  showDocumentationPage: false });
  }

  renderAction() {
    console.log(this.state.docList)
    return (
      <>
        {this.state.docList.map((val, idx) => {
          return (
              <tr key={val.id}>
                <td>{idx + 1}</td>
                <td>{val.type}</td>
                <td>{val.template}</td>
                <td>
                  <Link to={{pathname: `/adminsite/editDoctype/${val.id}`, state : {
                    idedit: val.id, templateedit: val.template, typeedit: val.type
                  }}}>
                    <button className="btn btn-success">
                      <i className="fa fa-edit"> Edit</i>
                    </button>
                  </Link>
                  <button className="btn btn-danger ml-2">
                    <i
                      className="fa fa-trash"
                      onClick={() => this.onBtnDelete(val.id)}
                    >
                      {" "}
                      Delete
                    </i>
                  </button>
                </td>
              </tr>
          );
        })}
      </>
    );
  }

  renderCategoryPage() {
    const { docList } = this.state;
    return (
      <div className="animated fadeIn">
        {!docList.length ? (
          <div className="animated fadeIn">
            <h2>Documentation Not Found</h2>
          </div>
        ) : (
          <div>
            <Card>
              <CardHeader>Document Type</CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="6" xl="6">
                    <NavLink to="/adminsite/addDocType" className="nav-link">
                      <button className="btn btn-primary">
                        <i className="fa fa-plus-circle">
                          {" "}
                          Add New Document Type
                        </i>
                      </button>
                    </NavLink>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="6" xl="6">
                    <Table
                      hover
                      responsive
                      className="table-outline mb-0 d-none d-sm-table"
                    >
                      <thead className="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Type</th>
                          <th>Template</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>{this.renderAction()}</tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </div>
        )}
      </div>
    );
  }

  renderDocumentationPage() {
    const { activeDocumentation, isLoading } = this.state;
    if (isLoading) {
      return this.loading();
    } else {
      if (
        activeDocumentation ||
        (!activeDocumentation && this.props.match.params.id === "add")
      )
        return (
          <>
            {isLoading ? (
              this.loading()
            ) : this.props.match.params.id === "add" ? (
              <AddDocType
                documentationData={activeDocumentation}
                {...this.props}
              />
            ) : (
              <EditDocType
                documentationData={activeDocumentation}
                {...this.props}
              />
            )}
          </>
        );
      else
        return (
          <div className="animated fadeIn">
            <h2>Documentation Data Not Found</h2>
          </div>
        );
    }
  }
  render() {
    console.log(this.props.match.params.id)
    const { showDocumentationPage } = this.state;
    
    if (showDocumentationPage) {
      return this.renderDocumentationPage();
    } else {
      return this.renderCategoryPage();
    }
  }
}

export default DocumentType;