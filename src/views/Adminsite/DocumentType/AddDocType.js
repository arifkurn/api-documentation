import React, { useState } from "react";
import dynamic from 'next/dynamic';
import 'react-quill/dist/quill.snow.css';
import Axios from "axios";
import swal from 'sweetalert';
import {Card,CardHeader, CardBody,CardFooter,Row,Col} from 'reactstrap'
import { API_UPLOAD_FILE, API_DOCTYPE, API_DOCUMENTATION } from "../../../Constants";
const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });


class AddDocType extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    
        this.state = {
          inputDocType: "",
          editorHtml: "",
          
        };
      }
      
    
      onChangeHandler = e => {
        this.setState({ [e.target.name]: e.target.value });
        console.log(e.target.value)
      };
    
    
      handleChange(html) {
        this.setState({ editorHtml: html });  
      }
    
    onSubmit=()=>{
      const { match: { params }, history } = this.props

      Axios.post(API_DOCTYPE,{
        type:this.state.inputDocType,
        template:this.state.editorHtml,
  
      }).then(res=>{
        console.log(res.data)
        swal({
          title: "Success",
          text: "Data Berhasil di Tambah",
          icon: "success",
          button: "OK",
        })
        .then(res=>{
          history.push(`/adminsite/documentType`)
        }) 
      }).catch(err=>{
        console.log(err)
      })
     
    }
    
      imageHandler() {
        const input = document.createElement("input");
    
        input.setAttribute("type", "file");
        input.setAttribute("accept", "image/*");
        input.click();
        const apiPostNewsImage = async (formData) => {
          return await Axios(
            {
              method: 'post',
              url: API_UPLOAD_FILE,
              data: formData,
              headers: {
              'content-type': `application/json`,
              }
            }
          ).then(function (response) {
            if ( typeof response.data.fileViewUri !== "undefined" ) {
              console.log("URI", response.data.fileViewUri)
              return response.data.fileViewUri
            } else {
              alert("Failed upload image to server")
              return null
            }
          })
          .catch(function (err) {
              alert("ERROR: "+err)
              return null
          });
        }
        input.onchange = async () => {
          const file = input.files[0];
          const formData = new FormData();
    
          formData.append("file", file);
    
          // Save current cursor state
          const range = this.quill.getSelection(true);
    
          // Insert temporary loading placeholder image
          this.quill.insertEmbed(
            range.index,
            "image",
            `${window.location.origin}/images/loaders/placeholder.gif`
          );
    
          // Move cursor to right side of image (easier to continue typing)
          this.quill.setSelection(range.index + 1);
    
          const res = await apiPostNewsImage(formData);
          // Remove placeholder image
          this.quill.deleteText(range.index, 1);
    
          // Insert uploaded image
          // this.quill.insertEmbed(range.index, 'image', res.body.image);
          this.quill.insertEmbed(range.index, "image", res);
        };
      }
    
      render() {
        return (
          <React.Fragment>
            <Card>
              <CardHeader>
                <h3>New Document Type</h3>
              </CardHeader>
              <CardBody >
                  <Row>
                  <Col xs="12">
                    <label>Type</label>
                    <input type="text" className="ml-3 form-control" name="inputDocType" onChange={this.onChangeHandler}/>
                  </Col>
                  </Row>
                  <Row>
                  <Col xs="12">
                  <label>Template</label>
                  <ReactQuill
                        theme="snow"
                        ref={el => {
                            this.quill = el;
                        }}
                        onChange={this.handleChange}
                        placeholder={this.props.placeholder}
                        modules={{
                            toolbar: {
                                container: [
                                    [{ header: '1' }, { header: '2' }, { header: [3, 4, 5, 6] }, { font: [] }],
                                    [{ size: [] }],
                                    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                                    [{ list: 'ordered' }, { list: 'bullet' }],
                                    ['link', 'video'],
                                    ['link', 'image', 'video'],
                                    ['clean'],
                                    ['code-block']
                                ],
                                handlers: {
                                    image: this.imageHandler
                                }
                            }
                        }}
                    />
                  </Col>
                  </Row>
              </CardBody>
              <CardFooter>
                <button className="btn btn-primary" onClick={this.onSubmit}>Add</button>
              </CardFooter>
            </Card>
          </React.Fragment>
        );
      }
    }

export default AddDocType;