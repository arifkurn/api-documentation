import React, { Component, lazy, Suspense } from 'react';
import {
    Card, CardHeader, CardBody,
    Row, Col, Collapse, Button, Form, Label, Input, FormText, FormGroup, Modal, ModalBody, ModalHeader, Table
} from 'reactstrap';
import axios from 'axios';
import { API_DOCUMENTATION, API_CATEGORY } from '../../../Constants';
import { Link } from 'react-router-dom';
import '../../../../node_modules/font-awesome/css/font-awesome.min.css'
import Axios from 'axios';
import swal from 'sweetalert';
import EditForm from './EditForm'
import Add from './Add'

class Documentation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            docList: [],
            isLoading: true,
            category_name: '',
            docName: null,
            docContent: null,
            id: null,
            category: null,
            doctype: null,
            description: '',
            collapse: false,
            status: null,
            isOpen: false,
            showCategoryPage: true,
            showDocumentationPage: false,
            activeDocumentation: false,
            item_category: null,
            modalIsOpen: false,
            description: '',
            idSelected: null,
            modalIsOpen: false,
            form_status: 'create',
            name_category: '',
            description_category: '',
            parent_id: null,
            data_name: null,
            data_desc: null,
            add_name: null
        }
    }

    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    handleModal = () => {
        this.setState({
            modalIsOpen: !this.state.modalIsOpen,
            form_status: 'create',
            name_category: '',
            description_category: ''
        })
    }

    addNewCategory = () => {
        let url = API_CATEGORY
        let payload = {
            category_name: this.state.data_name,
            description: this.state.data_desc,
            parent_id: this.props.match.params.category
        }
        axios.post(url, payload)
            .then(response => {
                window.location.href = "/adminsite/category/"
                return false
            })
            .catch(error => {
                console.log(error)
                return false
            })
    }


    getNewcategory = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitNewCategory = (e) => {
        e.preventDefault()
        this.setState({
            modalIsOpen: !this.state.modalIsOpen
        })
        if (this.state.form_status === 'create') {
            return this.addNewCategory()
        }
        else {
            return this.editCategory()
        }
    }

    editButton = () => {
        this.setState({
            name_category: this.state.category_name,
            description_category: this.state.description,
            form_status: 'edit',
            modalIsOpen: !this.state.modalIsOpen,
            idSelected: this.state.id,
            parent_id: this.props.match.params.category,
        })
    }

    editCategory = () => {
        let data_category = this.props.getCategorybyID(this.props.match.params.category)
        let url = API_CATEGORY + `?id=${this.props.match.params.category}`
        let payload = {
            category_name: this.state.add_name,
            description: this.state.data_desc,
            parent_id: data_category.parent_id
        }
        axios.put(url, payload)
            .then(response => {
                window.location.href = "/adminsite/documentation/"
                return false
            })
            .catch(error => {
                console.log(error)
                return false
            })
    }


    fetchingData() {
        const { showDocumentationPage } = this.state
        let data_category = this.props.getCategorybyID(this.props.match.params.category)
        let url = `${API_DOCUMENTATION}?category=${this.props.match.params.category}`
        console.log(this.props.match.params.category, 'fetchData')
        axios.get(url)
            .then(response => {
                console.log('fetch data', response.data)
                let activeDocumentationData = false
                if (showDocumentationPage) {
                    const indexDoc = this.props.match.params.id
                    activeDocumentationData = typeof response.data[indexDoc] === "undefined" ? false : response.data[indexDoc]
                    if (activeDocumentationData)
                        this.props.setActiveDocuments(activeDocumentationData.name)
                    else if (indexDoc === "add")
                        this.props.setActiveDocuments("Add New Documentation")
                    else
                        this.props.setActiveDocuments("404")
                }

                this.setState({
                    docList: response.data,
                    category_name: (data_category.category_name || null),
                    description: (data_category.description || null),
                    doctype: (data_category.doctype || null),
                    isLoading: false,
                    activeDocumentation: activeDocumentationData,
                    data_name: (data_category.category_name || null),
                    data_desc: (data_category.description || null),
                    add_name: (data_category.category_name || null)
                })
            })
            .catch(error => {
                this.setState({
                    docList: [],
                    category_name: null,
                    description: null,
                    doctype: null,
                    isLoading: false
                });
                console.log(`parsing data from ${API_DOCUMENTATION} failed`, error);
            })
    }

    deleteButton = (id) => {
        swal({
            title: "Are you sure?",
            text: "Apakah Anda Yakin Akan Menghapus Category ini ??",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    Axios.delete(API_CATEGORY + `?id=${this.props.match.params.category}`)
                        .then(res => {
                            this.fetchingData()
                            console.log(res.data)
                        }).catch(err => {
                            console.log(err)
                        })
                    swal("Data berhasil di hapus", {
                        icon: "success",
                    })
                        .then(res => {
                            window.location.href = "/adminsite/documentation/"
                        })
                } else {
                    swal("Data berhasil di simpan");
                }
            });
    }

    onBtnDelete = (id) => {
        swal({
            title: "Are you sure?",
            text: "Apakah Anda Yakin Akan Menghapus Dokumentasi ini ??",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    Axios.delete(API_DOCUMENTATION + '?id=' + id)
                        .then(res => {
                            this.fetchingData()
                            console.log(res.data)
                        }).catch(err => {
                            console.log(err)
                        })
                    swal("Data berhasil di hapus", {
                        icon: "success",
                    });
                } else {
                    swal("Data berhasil di simpan");
                }
            });
    }

    componentDidMount() {
        const documentationID = this.props.match.params.id
        if (typeof documentationID !== "undefined" && documentationID !== null)
            this.setState({ showDocumentationPage: true, showCategoryPage: false }, () => {
                this.fetchingData();
                this.props.chooseCategory(this.props.match.params.category)
            })
        else
            this.setState({ showDocumentationPage: false, showCategoryPage: true }, () => {
                this.fetchingData();
                this.props.chooseCategory(this.props.match.params.category)
            })
        this.fetchingData();
        this.props.chooseCategory(this.props.match.params.category);
    }

    componentDidUpdate(prevProps) {
        const { docList } = this.state
        const documentationID = this.props.match.params.id
        const categoryID = this.props.match.params.category
        if (typeof documentationID !== "undefined" && documentationID !== null) {
            if (this.props.match.params.id !== prevProps.match.params.id) {
                let activeDocumentationData = false
                let isLoading = true
                if (this.props.match.params.category === prevProps.match.params.category) {
                    activeDocumentationData = typeof docList[documentationID] === "undefined" ? false : docList[documentationID]
                    if (activeDocumentationData)
                        this.props.setActiveDocuments(activeDocumentationData.name)
                    else if (documentationID === "add")
                        this.props.setActiveDocuments("Add New Documentation")
                    else if (documentationID === "edit")
                        this.props.setActiveDocuments("404")

                    isLoading = false
                } else {
                    this.props.setActiveDocuments(null)
                }
                this.setState({ isLoading: isLoading, activeDocumentation: activeDocumentationData, showDocumentationPage: true, showCategoryPage: false }, () => {
                    if (this.props.match.params.category !== prevProps.match.params.category) {
                        this.fetchingData();
                        this.props.chooseCategory(this.props.match.params.category);
                        this.props.getCategorybyID(this.props.match.params.category)
                    }
                })
            } else if (this.props.match.params.category !== prevProps.match.params.category) {
                this.fetchingData();
                this.props.chooseCategory(this.props.match.params.category);
            }
        } else if (this.props.match.params.id !== prevProps.match.params.id) {
            this.props.setActiveDocuments(null)
            this.setState({ showDocumentationPage: false, showCategoryPage: true }, () => {
                if (this.props.match.params.category !== prevProps.match.params.category) {
                    this.fetchingData();
                    this.props.chooseCategory(this.props.match.params.category);
                }
            })
        } else if (this.props.match.params.category !== prevProps.match.params.category) {
            this.fetchingData();
            this.props.chooseCategory(this.props.match.params.category);
        }
    }


    UNSAFE_componentWillMount() {
        const documentationID = this.props.match.params.id
        if (typeof documentationID !== "undefined" && documentationID !== null)
            this.setState({ showCategoryPage: false, showDocumentationPage: true })
        else
            this.setState({ showCategoryPage: true, showDocumentationPage: false })
    }

    renderForm() {
        console.log(this.state.name_category)
        return (
            <div>
                <Button color='primary' onClick={this.handleModal} className='mb-4' style={{ marginLeft: '20px' }}> <i className="fa fa-plus-circle"> Add New Category</i></Button>
                <Modal isOpen={this.state.modalIsOpen} toggle={this.handleModal} >
                    <ModalHeader toggle={this.handleModal}>{this.state.form_status === 'create' ? 'Add New Category' : 'Edit Category'}</ModalHeader>
                    <ModalBody>
                        {this.state.form_status === 'create' ?
                            <Form onSubmit={this.submitNewCategory}>
                                <FormGroup>
                                    <Label>Category Name</Label>
                                    <Input type="text" name="data_name" placeholder="Enter category name" onChange={this.getNewcategory} required />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Description</Label>
                                    <Input type="textarea" name="data_desc" placeholder='Give description of category here...' onChange={this.getNewcategory} required />
                                </FormGroup>
                                <Button color='primary' type='submit'>Submit</Button>
                            </Form>
                            :
                            <Form onSubmit={this.submitNewCategory}>
                                <FormGroup>
                                    <Label>Category Name</Label>
                                    <Input type="text" name="add_name" value={this.state.add_name} placeholder="Enter category name" onChange={this.getNewcategory} required />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Description</Label>
                                    <Input type="textarea" name="description" value={this.state.description} placeholder='Give description of category here...' onChange={this.getNewcategory} required />
                                </FormGroup>
                                <Button color='primary' type='submit'>Submit</Button>
                            </Form>

                        }

                    </ModalBody>
                </Modal>
            </div>
        )
    }

    renderAction() {
        return (
            <> {this.state.docList.map((item, index) => {
                return (
                    <>
                        <ul className='mt-3'>
                            <li>
                                {item.name}
                                <span className="float-right">
                                    <Link to={`/adminsite/documentation/${this.props.match.params.category}/${index}`}>
                                        <button className="btn btn-success">
                                            <i className="fa fa-edit"> Edit</i>
                                        </button>
                                    </Link>
                                    &emsp;
                                    <button className="btn btn-danger">
                                        <i className="fa fa-trash" onClick={() => this.onBtnDelete(item.id)}> Delete</i>
                                    </button>
                                </span>

                            </li>
                        </ul>
                    </>
                )
            })} </>)
    }

    renderCategoryPage() {
        const { docList, category_name, isLoading } = this.state
        return (
            <div className="animated fadeIn">
                {isLoading ? this.loading() :

                    (
                        <div className='animated fadeIn'>
                            <CardHeader key="0">
                                <div className="row">
                                    <div className="col">
                                        <h3 style={{ marginLeft: 30, marginTop: 10 }}>{category_name}</h3>
                                    </div>
                                    <div style={{ marginTop: 10, marginLeft: '10%', paddingRight: 20 }}>

                                        <div onClick={() => this.editButton()}>
                                            <img src={require("../../../assets/edit.png")} style={{ width: 20, height: 20, right: '10%' }} />
                                        </div>
                                        <div onClick={() => this.deleteButton()}>
                                            <img src={require("../../../assets/trash.png")} style={{ width: 20, height: 20, marginLeft: '40px', marginTop: '-45px' }} />
                                        </div>
                                    </div>
                                    <div>
                                        <p style={{ marginTop: '60px', marginBottom: '2rem', marginLeft: '-918px', }}>{this.state.description}</p>
                                    </div>
                                </div>

                                <div className='row'>
                                    <Link to={`/adminsite/documentation/${this.props.match.params.category}/add`}>
                                        <Button color="primary" style={{ marginLeft: '45px' }}>
                                            <i className="fa fa-plus-circle"> Add New Documentation</i>
                                        </Button>
                                    </Link>
                                    {this.renderForm()}
                                </div>

                            </CardHeader>

                            {!docList.length ?
                                (<div className='animated fadeIn'>
                                    <h2>Documentation Not Found</h2>
                                </div>) : (<>
                                    <Card>
                                        <Table bordered className='bordered'>
                                            <thead>
                                                <tr>
                                                    <th>Documentation Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{this.renderAction()}</td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </Card>
                                </>
                                )}
                        </div>
                    )
                }
            </div>
        );
    }

    renderDocumentationPage() {

        const { activeDocumentation, isLoading } = this.state 
        if (isLoading) {
            return this.loading()
        } else {
            if (activeDocumentation || (!activeDocumentation && this.props.match.params.id === 'add'))
                return (
                    <>
                        {isLoading ? this.loading() : this.props.match.params.id === 'add' ? (<Add documentationData={activeDocumentation} {...this.props} />) : (<EditForm documentationData={activeDocumentation} {...this.props} />)}
                    </>
                )
            else
                return (
                    <div className='animated fadeIn'>
                        <h2>Documentation Data Not Found</h2>
                    </div>
                )
        }
    }

    render() {
        const { showCategoryPage, showDocumentationPage } = this.state
       
        if (showDocumentationPage) {
            return this.renderDocumentationPage()
        } else {
            return this.renderCategoryPage()
        }
        return this.renderForm()

    }

}

export default Documentation;
