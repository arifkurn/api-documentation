import React, { Component, lazy, Suspense } from 'react';
import {
  Card, CardHeader, CardBody,
  Row, Col, Button, Form, FormGroup, Label, Input, Modal, ModalBody, ModalHeader, ModalFooter, Alert
} from 'reactstrap';
import {API_CATEGORY} from '../../../Constants'
import { MDBDataTable } from 'mdbreact';
import axios from 'axios'



class Category extends Component {
	constructor(props){
		super(props)
		this.state = {
			category: [],
			category_name: '',
			description: '',
			form_status: 'create',
			idSelected: null,
			modalIsOpen: false,
		}
	}


  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  componentDidMount(){
  	this.fetchingData()
  }

  fetchingData(){
  	fetch(API_CATEGORY).then(res => res.json())
  	.then(parsedJSON => this.setState({category: parsedJSON}))
  	.catch(error => console.log(error))
  }

  handleModal = () => {
  	this.setState({
  		modalIsOpen: ! this.state.modalIsOpen,
  		form_status: 'create',
  		category_name: '',
  		description: ''
  	})
  }

  editButton = (index) => {
  	this.setState({
  		category_name: this.state.category[index].category_name,
  		description: this.state.category[index].description,
  		form_status: 'edit',
  		modalIsOpen: !this.state.modalIsOpen,
  		idSelected: this.state.category[index].id,
  	})
  }

  deleteButton = (id) => {
    var x = window.confirm(`Are you sure you want to delete ${this.state.category[id].category_name} category?`)
    if(x) {
      let url = API_CATEGORY + `?id=${this.state.category[id].id}`
        axios.delete(url)
        .then(response => {
          this.fetchingData()
        })
        .catch(error => {console.log(error)})
    }
    else {
      return false
    }
  }

  dataTable(){
  	const {category} = this.state
  	const rowData = () => {
  		return (
  			category.map((data, index) => ({
  			  category_name: data.category_name,
  			  description: data.description,
  			  edit_button: <div className='text-right'><Button color='success' className='m-auto btn-sm' onClick={() => this.editButton(index)}>Edit</Button></div>,
  			  delete_button: <Button color='danger' className='btn-sm m-auto' onClick={() => this.deleteButton(index)}>Delete</Button>
  			}))
  		)
  	}

  	const data = {
  		columns: [
  			{
  				label: 'Category',
  				field: 'category_name',
  				sort: 'asc'
  			},
  			{
  				label: 'Description',
  				field: 'description',
  				sort: 'asc'
  			},
  			{
  				label: '',
  				field: 'edit_button',
  				sort: 'asc'
  			},
  			{
  				label: '',
  				field: 'delete_button',
  				sort: 'asc'
  			}
  		],
  		rows: rowData()
  	}

  	return(
  		<MDBDataTable striped hover entries={5} data={data} searching={false} displayEntries={false} responsive sortable={false}/>
  	)
  }

 
  getNewcategory = (event) => {
    this.setState({
      [event.target.name] : event.target.value
    })
  }

  addNewCategory = () => {
    let url = API_CATEGORY
    let payload = {
      category_name: this.state.category_name,
      description: this.state.description,
    }
  	axios.post(url, payload)
  		.then(response => {
  			this.fetchingData()
  			return false
  		})
  		.catch(error => {
  			console.log(error)
  			return false
  		})
  }

  editCategory = () => {
    let url = API_CATEGORY + `?id=${this.state.idSelected}`
    let payload = {
      category_name: this.state.category_name,
      description: this.state.description,
    }
  	axios.put(url, payload)
  		.then(response => {
        this.fetchingData()
  			return false
  		})
  		.catch(error => {
  			console.log(error)
  			return false
  		})
  }

  
  submitNewCategory = (e) => {
  	e.preventDefault()
  	this.setState({
  		modalIsOpen: ! this.state.modalIsOpen
  	})
  	if (this.state.form_status === 'create'){
  		return this.addNewCategory()
  	}
  	else{
  		return this.editCategory()
  	}
  }
  renderForm(){
  	return(
  		<div>
  			<Button color='primary' onClick={this.handleModal} className='mb-4'>Add New Category</Button>
	      	<Modal isOpen={this.state.modalIsOpen} toggle={this.handleModal} >
	        	<ModalHeader toggle={this.handleModal}>{this.state.form_status === 'create' ? 'Add New Category' : 'Edit Category'}</ModalHeader>
	        		<ModalBody>
	        			<Form onSubmit={this.submitNewCategory}>
						  		<FormGroup>
						        <Label>Category Name</Label>
						        <Input type="text" name="category_name" value={this.state.category_name} placeholder="Enter category name"  onChange={this.getNewcategory} required/>
						      </FormGroup>
						      <FormGroup>
						        <Label>Description</Label>
						        <Input type="textarea" name="description" value={this.state.description} placeholder='Give description of category here...' onChange={this.getNewcategory} required/>
						      </FormGroup>
						      <Button color='primary' type='submit'>Submit</Button>
						  	</Form> 	        			
				  	</ModalBody>
	      	</Modal>
  		</div>
  	)
  }

  render() {
    return (
      <div className="animated fadeIn">
      	{this.renderForm()}
      	{this.dataTable()}
      </div>
    );
  }
}

export default Category;
