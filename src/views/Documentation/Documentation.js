import React, { Component, lazy, Suspense } from 'react';
import {
  Card, CardHeader, CardBody,
  Row, Col
} from 'reactstrap';
import { API_DOCUMENTATION } from '../../Constants'
import axios from 'axios'
import { Link, HashRouter } from 'react-router-dom';
import Detil from './Detil.js'

class Documentation extends Component {
	constructor(props){
		super(props)
		this.state = {
			apiList: [],
			category_name: null,
			description: null,
			doctype: null,
			isLoading: true,
			showDocumentationPage: false,
			showCategoryPage: true,
			activeDocumentation: false
		}
	}

	loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

	componentDidUpdate(prevProps) {
		const {apiList} = this.state
		const documentationID = this.props.match.params.id
		const categoryID = this.props.match.params.category
		if ( typeof documentationID !== "undefined" && documentationID !== null ) {
			if (this.props.match.params.id !== prevProps.match.params.id) {
				let activeDocumentationData = false
	  			let isLoading = true
	  			if (this.props.match.params.category === prevProps.match.params.category) {
		  			activeDocumentationData = typeof apiList[documentationID] === "undefined" ? false: apiList[documentationID]
		  			if ( activeDocumentationData )
		  				this.props.setActiveDocuments(activeDocumentationData.name)
		  			else 
		  				this.props.setActiveDocuments("A 404")

		  			isLoading = false
		  		} else {
		  			this.props.setActiveDocuments(null)
		  		}
		  		this.setState({isLoading: isLoading, activeDocumentation: activeDocumentationData, showDocumentationPage: true, showCategoryPage: false}, () => {
					if (this.props.match.params.category !== prevProps.match.params.category) {
						this.fetchData();
						this.props.chooseCategory(this.props.match.params.category);
					}
				})
			} else if (this.props.match.params.category !== prevProps.match.params.category) {
				this.fetchData();
				this.props.chooseCategory(this.props.match.params.category);
			}
		} else if (this.props.match.params.id !== prevProps.match.params.id) {
			this.props.setActiveDocuments(null)
			this.setState({showDocumentationPage: false, showCategoryPage: true}, () => {
				if (this.props.match.params.category !== prevProps.match.params.category) {
					this.fetchData();
					this.props.chooseCategory(this.props.match.params.category);
				}
			})
		} else if (this.props.match.params.category !== prevProps.match.params.category) {
			this.fetchData();
			this.props.chooseCategory(this.props.match.params.category);
		}
	}
	componentDidMount(){
		const documentationID = this.props.match.params.id
		if ( typeof documentationID !== "undefined" && documentationID !== null ) 
			this.setState({showDocumentationPage: true, showCategoryPage: false}, () => {
				this.fetchData();
				this.props.chooseCategory(this.props.match.params.category)
			})
		else
			this.setState({showDocumentationPage: false, showCategoryPage: true}, () => {
				this.fetchData();
				this.props.chooseCategory(this.props.match.params.category)
			})
	}

	fetchData(){
		const {showDocumentationPage} = this.state
		let url = `${API_DOCUMENTATION}?category=${this.props.match.params.category}`
		// console.log(this.props.match.params.category, 'fetchData')
		axios.get(url)
		.then(response => {
			let activeDocumentationData = false
			if ( showDocumentationPage ) {
				const indexDoc = this.props.match.params.id
				activeDocumentationData = typeof response.data[indexDoc] === "undefined" ? false: response.data[indexDoc]
				if ( activeDocumentationData )
					this.props.setActiveDocuments(activeDocumentationData.name)
				else
					this.props.setActiveDocuments("B 404")
			}
			this.setState({
				apiList: response.data,
				category_name: response.data.length ? response.data[0].cat.category_name: null,
				description: response.data.length ? response.data[0].cat.description: null,
				doctype: response.data.length ? response.data[0].doc.type: null,
				isLoading: false,
				activeDocumentation: activeDocumentationData,
			})
		})
		.catch(error => {
			this.setState({
	       apiList: [],
	       category_name: null,
	       description: null,
	       doctype: null,
	       isLoading: false
	    });
	    console.log(`parsing data from ${API_DOCUMENTATION} failed`, error);
		})
	}
	renderCategoryPage() {
		const { apiList, category_name, isLoading } = this.state
	    return (
			<HashRouter>
	      <div className="animated fadeIn">
	      	{ isLoading ? this.loading() : !apiList.length ? 
	      		(<div className='animated fadeIn'>
	      		  <h2>Documentation Not Found</h2>
	      		 </div>) :
	      		(
	      			<div className='animated fadeIn'>
	      				<Card>
	      					<CardHeader>
			      				<h3>{this.state.category_name}</h3>
	      					</CardHeader>
	      					<CardBody>
								{/* {this.state.description === null ? null : <Row><Col>{this.state.description}</Col></Row>} */}
								<ul className='mt-3'>
									{
										apiList.map((item, index)=> {
			      							return(
												<li role='button' onClick={()=>this.props.setActiveDocuments(item.name)}>
													<Link to={`/documentation/${this.props.match.params.category}/${index}`}>{item.name}</Link>
												</li>
											)
										})
									}
								</ul>
							</CardBody>
	      				</Card>
	      			</div>
	      		)
	      	}
	      </div>
		  </HashRouter>
	    );
	}
	renderDocumentationPage() {
		const {activeDocumentation, isLoading} = this.state
		if ( isLoading ) {
			return this.loading()
		} else {
			if ( activeDocumentation )
				return (
					<Suspense>
						<Detil documentationData={activeDocumentation} />
					</Suspense>
				)
			else
				return (
					<HashRouter>
					<div className='animated fadeIn'>
		      		  <h2>Documentation Data Not Found</h2>
		      		</div>
					  </HashRouter>
				)
		}
	}
	render() {
		const {showCategoryPage, showDocumentationPage} = this.state
		if ( showDocumentationPage )
			return this.renderDocumentationPage()
		else
			return this.renderCategoryPage()
	}
}
export default Documentation;