import React, { Component, lazy, Suspense } from 'react';
import {
  Card, CardHeader, CardBody,
  Row, Col, Button
} from 'reactstrap';
import { API_DOCUMENTATION } from '../../Constants'
import axios from 'axios'
import { Link, HashRouter } from 'react-router-dom';
import HtmlToReact from 'html-to-react'

class Detil extends Component {
	constructor(props){
		super(props)
		this.state = {
			
		}
	}

	loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

	render() {
		var HtmlToReactParser = HtmlToReact.Parser
		var HtmlToReactParser = new HtmlToReactParser()
		const {apiList} = this.state
		const item = this.props.documentationData
		return (
			<HashRouter>
			<div className='animated fadeIn'>
				<Card>
					<CardHeader>
						<h3>{item.name}</h3>
					</CardHeader>
					<CardBody className="documentation_content">
						{HtmlToReactParser.parse(item.content)}
					</CardBody>
				</Card>

			</div>
			</HashRouter>
		)
	}
}
export default Detil