import React, { Component, lazy, Suspense } from 'react';
import {
  Card, CardHeader, CardBody,
  Row, Col, Button
} from 'reactstrap';
import { APP_TITLE, API_DOCUMENTATION } from '../../Constants'
import { getUrlParameter } from '../../Helper'
import axios from 'axios'
import { Link, HashRouter } from 'react-router-dom'

class Search extends Component {
	constructor(props){
		super(props)
		this.state = {
			activeKeyword: getUrlParameter("keyword"),
			searchResult: [],
			isLoading: true
		}
	}
	
	loading = () => <div className="animated fadeIn pt-1 text-center">Searching Documents ...</div>

	componentDidUpdate(prevProps) {
		const {activeKeyword} = this.state
		let keyword = getUrlParameter("keyword")
		let isLoading = true
		if ( activeKeyword !== keyword ) {
			this.setState({activeKeyword: keyword, isLoading: true}, () => {
				this.fetchData(keyword)
			})
		} else if (this.props.reload !== prevProps.reload && this.props.reload ) {
			this.setState({activeKeyword: keyword, isLoading: true}, () => {
				this.fetchData(keyword)
			})
		}
	}
	componentDidMount() {
		const {activeKeyword} = this.state
		this.props.setDocumentTitle("Search Result | " + APP_TITLE)
		this.props.setBreadcrump([{
			label: "Searching Documents",
			link: null
		}])
		this.fetchData(activeKeyword)
	}
	fetchData ( keyword ) {
		let url = `${API_DOCUMENTATION}?docName=${escape(keyword)}`
		// console.log(this.props.match.params.category, 'fetchData')
		axios.get(url)
		.then(response => {
			this.setState({
				searchResult: response.data.length ? response.data: [],
				isLoading: false
			})
		})
		.catch(error => {
			this.setState({
	    		searchResult: [],
				isLoading: false
	    	});
	    	console.log(`parsing data from ${API_DOCUMENTATION} failed`, error);
		})
	}

	render() {
		const {activeKeyword, searchResult, isLoading} = this.state
		return (
			<HashRouter>
			isLoading ? this.loading(): (
				<div className='animated fadeIn'>
					<Card>
						<CardHeader>
							<h3>{searchResult.length} Results</h3>
						</CardHeader>
						<CardBody>
							{
								searchResult.length ? (
									<ul>
										{
											searchResult.map((item, index) => {
												return (
													<li key={index}><Link to={`/documentation/${item.cat.id}/${index}`}>{item.name}</Link></li>
												)
											})
										}
									</ul>
								): (
									<span>Data Not Found</span>
								)
							}
						</CardBody>
					</Card>

				</div>
				
		)
		</HashRouter>
	)
	}
}
export default Search