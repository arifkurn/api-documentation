import React from 'react';
import ReactDOM from 'react-dom';
import ApiDocumentation from './ApiDocumentation';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ApiDocumentation />, div);
  ReactDOM.unmountComponentAtNode(div);
});
