import React, { Component, lazy, Suspense } from "react";
import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";
import { API_DOC_LIST } from "../../Constants";
import { HashRouter } from 'react-router-dom';

class ApiDocumentation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiList: [],
      category_name: null,
      isLoading: true
    };
  }

  loadApiDocumentation() {
    fetch(`${API_DOC_LIST}?category=${this.props.match.params.id}`)
      .then(res => res.json())
      .then(parsedJSON => {
        if (parsedJSON.length) {
          this.setState({
            apiList: parsedJSON,
            category_name: parsedJSON[0].cat.category_name,
            isLoading: false
          });
        } else {
          this.setState({
            apiList: [],
            category_name: null,
            isLoading: false
          });
        }
      })
      .catch(error => {
        this.setState({
          apiList: [],
          category_name: null,
          isLoading: false
        });
        console.log(`parsing data from ${API_DOC_LIST} failed`, error);
      });
  }

  componentDidMount() {
    this.loadApiDocumentation();
    this.props.chooseCategory(this.props.match.params.id)
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.loadApiDocumentation();
      this.props.chooseCategory(this.props.match.params.id)
    }
  }

  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  render() {
    const { apiList, isLoading, category_name } = this.state;
    return (
      <HashRouter>
      <div className="animated fadeIn">
        {isLoading ? (
          this.loading()
        ) : !isLoading && !apiList.length ? (
          <div className="animated fadeIn">
            <h2>Documentation Not Found</h2>
          </div>
        ) : (
              <div className="animated fadeIn">
                <Row>
                  <Col xs="12" lg="12">
                    {apiList.map((apiDoc, index) => {
                      return (
                        <Card key={index}>
                          <CardHeader>
                            <h3>{apiDoc.name}</h3>
                          </CardHeader>
                          <CardBody>
                            <div className="container">
                              <div className="row">
                                <div className="col-4">
                                  <b>Method</b>
                                </div>
                                <div className="col-4">
                                  <b>Endpoint</b>
                                </div>
                                <div className="col-4">
                                  <b>Usage</b>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-4">{apiDoc.method}</div>
                                <div className="col-4">{apiDoc.url}</div>
                                <div className="col-4">{apiDoc.description}</div>
                              </div>
                              {
                                apiDoc.headers.length > 0 ? (<> <div className="row">
                                  <div className="col">
                                    <h5>Headers</h5>
                                  </div>
                                </div>
                                  <div className="row">
                                    <div className="col-4">
                                      <b>Key</b>
                                    </div>
                                    <div className="col-4">
                                      <b>Value</b>
                                    </div>
                                  </div>
                                  {apiDoc.headers.map((val, i) => {
                                    return (
                                      <div className="row" key={i}>
                                        <div className="col-4">{val.key}</div>
                                        <div className="col-4">{val.value}</div>
                                      </div>
                                    )
                                  })}</>) : null}
                              {apiDoc.params.length > 0 ? (<> <div className="row mt-3">
                                <div className="col">
                                  <h5>Params</h5>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-4">
                                  <b>Name</b>
                                </div>
                                <div className="col-4">
                                  <b>Type</b>
                                </div>
                                <div className="col-4">
                                  <b>Description</b>
                                </div>
                              </div>
                              {apiDoc.params.map((val, i) => {
                                return (
                                  <div className="row" key={i}>
                                    <div className="col-4">
                                      {val.name}
                                    </div>
                                    <div className="col-4">
                                      {val.type}
                                    </div>
                                    <div className="col-4">
                                      {val.description}
                                    </div>
                                  </div>
                                )
                              })} </> ) : null}
                             {apiDoc.payload.length > 0 ? (<> <div className="row mt-3">
                                <div className="col">
                                  <h5>Payload</h5>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-4">
                                  <b>Name</b>
                                </div>
                                <div className="col-4">
                                  <b>Type</b>
                                </div>
                                <div className="col-4">
                                  <b>Description</b>
                                </div>
                              </div>
                              {apiDoc.payload.map((val) => {
                                return (
                                  <div className="row">
                                    <div className="col-4">
                                      {val.name}
                                    </div>
                                    <div className="col-4">
                                      {val.type}
                                    </div>
                                    <div className="col-4">
                                      {val.description}
                                    </div>
                                  </div>
                                )
                              })} </> ) : null}
                              
                              <div className="row mt-3">
                                <div className="col">
                                  <h5>JSON Payload</h5>
                                </div>
                              </div>
                              <div className="row" style={{ whiteSpace: 'pre-wrap' }}>
                                <div className="col">
                                  {apiDoc.json_payload}
                                </div>
                              </div>
                              <div className="row mt-3">
                                <div className="col">
                                  <h5> JSON Response</h5>
                                </div>
                              </div>
                              <div className="row" style={{ whiteSpace: 'pre-wrap' }}>
                                <div className="col">
                                  {apiDoc.json_response}
                                  {apiDoc.response.length > 0 ? (<> <div className="row mt-3">
                                    <div className="col">
                                      <h5>Response</h5>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-4">
                                      <b>Name</b>
                                    </div>
                                    <div className="col-4">
                                      <b>Type</b>
                                    </div>
                                    <div className="col-4">
                                      <b>Description</b>
                                    </div>
                                  </div>
                                  {apiDoc.response.map((val) => {
                                    return (
                                      <div className="row">
                                        <div className="col-4">
                                          {val.name}
                                        </div>
                                        <div className="col-4">
                                          {val.type}
                                        </div>
                                        <div className="col-4">
                                          {val.description}
                                        </div>
                                      </div>
                                    )
                                  })}</> ) : null}
                                  
                                </div>
                              </div>
                            </div>
                          </CardBody>
                        </Card>
                      );
                    })}
                  </Col>
                </Row>
              </div>
            )}
      </div>
      </HashRouter>
    );
  }
}

export default ApiDocumentation;

