import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { HashRouter } from 'react-router-dom';

class Forgetpassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formMessage: false,
      email: ''
    }
  }
  handleForgetpassword = (e) => {
    e.preventDefault()
    var email = this.state.email;
    /*Show success message if email has been submited*/
    this.setState({
      formMessage: true
    });
  }
  formHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }
  render() {
    return (
      <HashRouter>
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  {
                    this.state.formMessage ? <div><p>We have sent a reset password link to your email :</p>
                      <p className="text-center"><strong>{this.state.email}</strong></p>
                      <span>Please kindly check your email</span>. <Link to="/login">Login</Link>
                      </div>
                    : <Form onSubmit={this.handleForgetpassword}>
                      <h1>Forget Password</h1>
                      <p className="text-muted">Please insert your email address</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-envelope"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="email" name="email" placeholder="Email" autoComplete="email" onChange={(ev) => this.formHandler(ev)} />
                      </InputGroup>
                      <Button color="success" block>Send Link</Button>
                    </Form>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
      </HashRouter>
    );
  }
}

export default Forgetpassword;
