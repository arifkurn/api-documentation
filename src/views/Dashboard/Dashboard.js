// import React, { useState } from "react";
// import dynamic from 'next/dynamic';
// import 'react-quill/dist/quill.snow.css';
// import Axios from "axios";
// import { API_UPLOAD_FILE, API_DOCTYPE, API_DOCUMENTATION } from "../../Constants";
// const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });

// class MyComponent extends React.Component {
//   constructor(props) {
//     super(props);
//     this.handleChange = this.handleChange.bind(this);

//     this.state = {
//       docType: [],
//       inputNameAdd: "",
//       selectDocTypeAdd: 0,
//       editorHtml: '',
         
//     };
//   }
  
//   componentDidMount() {
//     this.getInitialData();
//   }

//   getInitialData = () => {
//     Axios.get(API_DOCTYPE)
//       .then(res => {
//         this.setState({ docType: res.data });
//         console.log(this.state.docType);
//       })
//       .catch(err => {
//         console.log(err);
//       });
//   };

//   renderListType = () => {
//     return this.state.docType.map(val => {
//       return (
//       <option key={val.id} value={val.id}>{val.type}</option>
//       );
//     });
//   };

//   onChangeHandler = e => {
//     this.setState({ [e.target.name]: e.target.value });
//     console.log(e.target.value)
//   };


//   handleChange(html) {
//     this.setState({ editorHtml: html });  
//   }

// onSubmit=()=>{
//   Axios.post(API_DOCUMENTATION,{
//     doctype:this.state.selectDocTypeAdd,
//     name:this.state.inputNameAdd,
//     content:this.state.editorHtml,
//     category: this.state.category
//   }).then(res=>{
//     console.log(res.data)
//   }).catch(err=>{
//     console.log(err)
//   })
  
// }

//   imageHandler() {
//     const input = document.createElement("input");

//     input.setAttribute("type", "file");
//     input.setAttribute("accept", "image/*");
//     input.click();
//     const apiPostNewsImage = async (formData) => {
//       return await Axios(
//         {
//           method: 'post',
//           url: API_UPLOAD_FILE,
//           data: formData,
//           headers: {
//           'content-type': `application/json`,
//           }
//         }
//       ).then(function (response) {
//         if ( typeof response.data.fileViewUri !== "undefined" ) {
//           console.log("URI", response.data.fileViewUri)
//           return response.data.fileViewUri
//         } else {
//           alert("Failed upload image to server")
//           return null
//         }
//       })
//       .catch(function (err) {
//           alert("ERROR: "+err)
//           return null
//       });
//     }
//     input.onchange = async () => {
//       const file = input.files[0];
//       const formData = new FormData();

//       formData.append("file", file);

//       // Save current cursor state
//       const range = this.quill.getSelection(true);

//       // Insert temporary loading placeholder image
//       this.quill.insertEmbed(
//         range.index,
//         "image",
//         `${window.location.origin}/images/loaders/placeholder.gif`
//       );

//       // Move cursor to right side of image (easier to continue typing)
//       this.quill.setSelection(range.index + 1);

//       const res = await apiPostNewsImage(formData);
//       // Remove placeholder image
//       this.quill.deleteText(range.index, 1);

//       // Insert uploaded image
//       // this.quill.insertEmbed(range.index, 'image', res.body.image);
//       this.quill.insertEmbed(range.index, "image", res);
//     };
//   }

//   render() {
//     return (
//       <React.Fragment>
//         <div className="row">
//           <div className="col-2">
//             <label>Document Type</label>
//           </div>
//           <div className="col-lg-6 col-md-6 col-sm-8 col-xs-10">
//             <select
//               className="ml-3 form-control"
//               name="selectDocTypeAdd"
//               onChange={this.onChangeHandler}
//             >
//               <option value={0}>--Pilih Type--</option>
//               {this.renderListType()}
//             </select>
//           </div>
//         </div>
//         <div className="row mt-2">
//           <div className="col-2">
//             <label>Name</label>
//           </div>
//           <div className="col-lg-6 col-md-6 col-sm-8 col-xs-10">
//             <input type="text" className="ml-3 form-control" name="inputNameAdd" onChange={this.onChangeHandler}/>
//           </div>
//         </div>
//         <div className="row">
//           <div className="col">Content</div>
//         </div>
//         <div className="row mt-2">
//           <div className="col">
//             <div className="text-editor">
//                 {JSON.stringify(this.state.editorHtml)}
//                 <hr />
//                 <ReactQuill
//                     theme="snow"
//                     ref={el => {
//                         this.quill = el;
//                     }}
//                     onChange={this.handleChange}
//                     placeholder={this.props.placeholder}
//                     modules={{
//                         toolbar: {
//                             container: [
//                                 [{ header: '1' }, { header: '2' }, { header: [3, 4, 5, 6] }, { font: [] }],
//                                 [{ size: [] }],
//                                 ['bold', 'italic', 'underline', 'strike', 'blockquote'],
//                                 [{ list: 'ordered' }, { list: 'bullet' }],
//                                 ['link', 'video'],
//                                 ['link', 'image', 'video'],
//                                 ['clean'],
//                                 ['code-block']
//                             ],
//                             handlers: {
//                                 image: this.imageHandler
//                             }
//                         }
//                     }}
//                 />
//             </div>
//           </div>
//         </div>
//         <div className="row mt-2">
//           <div className="col">
//             <button className="btn btn-primary" onClick={this.onSubmit}>Submit</button>
//           </div>
//         </div>
//       </React.Fragment>
//     );
//   }
// }

// export default MyComponent;
import React, { Component } from 'react';

class MyComponent extends Component {
  render() {
    return (
      <div>
        
      </div>
    );
  }
}

export default MyComponent;