import React from 'react';
const Dashboard = React.lazy(() => import('./views/Adminsite/Dashboard'));
const DocumentType = React.lazy(() => import('./views/Adminsite/DocumentType'));
const AddDocType = React.lazy(() => import('./views/Adminsite/DocumentType/AddDocType'));
const EditDocType = React.lazy(() => import('./views/Adminsite/DocumentType/EditDocType'));
const Category = React.lazy(() => import('./views/Adminsite/Category'));
const Documentation = React.lazy(() => import ('./views/Adminsite/Documentation'));
const Add = React.lazy(()=>import('./views/Adminsite/Documentation/Add'));
const EditForm =React.lazy(()=>import('./views/Adminsite/Documentation/EditForm'));
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/adminsite/', exact: true, name: 'Home' },
  { path: '/adminsite/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/adminsite/documentType', name: 'DocumentType', component: DocumentType },
  { path: '/adminsite/addDocType', name: 'AddDocType', component: AddDocType },
  { path: '/adminsite/editDoctype/:id', name: 'EditDocType', component: EditDocType },
  { path: '/adminsite/category', name: 'Category', component: Category },
  { path: '/adminsite/documentation/:category/:id?', name: 'Documentation', component: Documentation},
  { path:'/adminsite/documentation/:category/add', name:'AddDocumentation', component: Add},
  { path:'/adminsite/documentation/:category/edit', name:'EditForm', component: EditForm},
  
];

export default routes;